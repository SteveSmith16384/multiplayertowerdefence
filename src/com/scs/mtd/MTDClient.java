package com.scs.mtd;

import com.jme3.input.KeyInput;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.util.SkyFactory;
import com.scs.mtd.entities.ConstructionSelector;
import com.scs.mtd.entities.IConstructionSelector;
import com.scs.stevetech1.client.AbstractGameClient;
import com.scs.stevetech1.client.AbstractSimpleGameClient;
import com.scs.stevetech1.components.IEntity;
import com.scs.stevetech1.netmessages.NewEntityData;
import com.scs.stevetech1.server.Globals;

public class MTDClient extends AbstractSimpleGameClient {

	private static final String BUILD1 = "BUILD1";
	
	private MTDClientEntityCreator entityCreator;
	
	private MTDHUD hud;
	
	private IConstructionSelector currentSelector;
	private boolean showBuildSelector = false;

	public static void main(String[] args) {
		try {
			new MTDClient("localhost", MTDStaticData.DEFAULT_PORT, "My Name");
		} catch (Exception e) {
			Globals.p("Error: " + e);
			e.printStackTrace();
		}
	}


	private MTDClient(String gameIpAddress, int gamePort, String _playerName) {
		super("MTD", gameIpAddress, gamePort, _playerName);
		
		start();		
	}


	@Override
	public void simpleInitApp() {
		super.simpleInitApp();

		entityCreator = new MTDClientEntityCreator();
				
		hud = new MTDHUD(this, this.getCamera());
		this.getGuiNode().attachChild(hud);
		
		getInputManager().addMapping(BUILD1, new KeyTrigger(KeyInput.KEY_ESCAPE));
		getInputManager().addListener(this, BUILD1);            

	}


	@Override
	public void simpleUpdate(float tpf_secs) {
		super.simpleUpdate(tpf_secs);

		hud.processByClient(this, tpf_secs);
		
		if (showBuildSelector && this.currentSelector == null) {
			showBuildSelector = false;
			currentSelector = new ConstructionSelector();
		}
		if (currentSelector != null) {
			
		}
	}

	
	/**
	 * Don't run any major code in here since it runs in the UI thread!
	 */
	@Override
	public void onAction(String name, boolean value, float tpf) {
		if (name.equalsIgnoreCase(BUILD1)) {
			if (value) {
				showBuildSelector = true;
			}
		} else {
			super.onAction(name, value, tpf);
		}
	}
	
	
	@Override
	protected void allEntitiesReceived() {
		super.allEntitiesReceived();

		getGameNode().attachChild(SkyFactory.createSky(getAssetManager(), "Textures/BrightSky.dds", SkyFactory.EnvMapType.CubeMap));

	}


	@Override
	protected IEntity actuallyCreateEntity(AbstractGameClient client, NewEntityData msg) {
		return entityCreator.createEntity(client, msg);
	}


	@Override
	protected Class<? extends Object>[] getListofMessageClasses() {
		return new Class[] {MTDSimplePlayerData.class};
	}


	@Override
	protected void showDamageBox() {
		this.hud.showDamageBox();
	}


	@Override
	protected void showMessage(String msg) {
		this.hud.appendToLog(msg);
	}


	@Override
	protected void appendToLog(String msg) {
		this.hud.appendToLog(msg);
	}

	
}
