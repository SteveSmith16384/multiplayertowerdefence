package com.scs.mtd.weapons;

import com.jme3.math.Vector3f;
import com.scs.mtd.MTDClientEntityCreator;
import com.scs.mtd.entities.FireballBullet;
import com.scs.stevetech1.components.ICanShoot;
import com.scs.stevetech1.components.IEntity;
import com.scs.stevetech1.server.ClientData;
import com.scs.stevetech1.shared.IAbility;
import com.scs.stevetech1.shared.IEntityController;
import com.scs.stevetech1.weapons.AbstractMagazineGun;

public class MagicWand extends AbstractMagazineGun implements IAbility {

	private static final int MAG_SIZE = 999;

	public MagicWand(IEntityController game, int id, int playerID, ICanShoot owner, int avatarID, byte num, ClientData _client) {
		super(game, id, MTDClientEntityCreator.MAGIC_WAND, playerID, owner, avatarID, num, "MagicWand", 1, 3, MAG_SIZE, _client);
		
	}


	@Override
	protected FireballBullet createBullet(int entityid, int playerID, IEntity _shooter, Vector3f startPos, Vector3f _dir, byte side) {
		return new FireballBullet(game, entityid, playerID, _shooter, startPos, _dir, side, client);
	}
	

}

