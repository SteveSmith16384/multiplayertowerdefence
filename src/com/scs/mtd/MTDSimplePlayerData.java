package com.scs.mtd;

import com.jme3.network.serializing.Serializable;
import com.scs.stevetech1.data.SimplePlayerData;

@Serializable
public class MTDSimplePlayerData extends SimplePlayerData {

	public int score;
	public int resources;
	
	public MTDSimplePlayerData() {
		super();
	}
}
