package com.scs.mtd;

import java.io.IOException;

import com.jme3.math.Vector3f;
import com.jme3.system.JmeContext;
import com.scs.mtd.entities.Floor;
import com.scs.mtd.entities.Golem;
import com.scs.mtd.entities.InvisibleMapBorder;
import com.scs.mtd.entities.WizardServerAvatar;
import com.scs.mtd.weapons.MagicWand;
import com.scs.stevetech1.data.SimplePlayerData;
import com.scs.stevetech1.entities.AbstractAvatar;
import com.scs.stevetech1.entities.AbstractServerAvatar;
import com.scs.stevetech1.entities.PhysicalEntity;
import com.scs.stevetech1.jme.JMEAngleFunctions;
import com.scs.stevetech1.server.AbstractSimpleGameServer;
import com.scs.stevetech1.server.ClientData;
import com.scs.stevetech1.shared.IAbility;

import ssmith.lang.NumberFunctions;

public class MTDServer extends AbstractSimpleGameServer {

	private int mapSize = 20;

	public static void main(String[] args) {
		try {
			new MTDServer();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private MTDServer() throws IOException {
		super(MTDStaticData.DEFAULT_PORT);

		start(JmeContext.Type.Headless);
	}


	@Override
	public void moveAvatarToStartPosition(AbstractAvatar avatar) {
		avatar.setWorldTranslation(3+(avatar.getSide()*3), 3f, 3+(avatar.getSide()*3));
	}


	@Override
	protected void createGame() {
		Golem golem = new Golem(this, getNextEntityID(), 10f, 0f, 10f, AbstractAvatar.ANIM_IDLE);
		this.actuallyAddEntity(golem);

		// Place floor last so the entities don't collide with it when being placed
		Floor floor = new Floor(this, getNextEntityID(), 0, 0, 0, mapSize, .5f, mapSize, "Textures/mud.png");
		this.actuallyAddEntity(floor);

		// To make things easier for the server and client, we surround the map with a colliding but invisible wall.

		// Map border
		InvisibleMapBorder borderL = new InvisibleMapBorder(this, getNextEntityID(), 0, 0, 0, mapSize, Vector3f.UNIT_Z);
		this.actuallyAddEntity(borderL);
		InvisibleMapBorder borderR = new InvisibleMapBorder(this, getNextEntityID(), mapSize+InvisibleMapBorder.BORDER_WIDTH, 0, 0, mapSize, Vector3f.UNIT_Z);
		this.actuallyAddEntity(borderR);
		InvisibleMapBorder borderBack = new InvisibleMapBorder(this, getNextEntityID(), 0, 0, mapSize, mapSize, Vector3f.UNIT_X);
		this.actuallyAddEntity(borderBack);
		InvisibleMapBorder borderFront = new InvisibleMapBorder(this, getNextEntityID(), 0, 0, -InvisibleMapBorder.BORDER_WIDTH, mapSize, Vector3f.UNIT_X);
		this.actuallyAddEntity(borderFront);

	}


	private void addEntityToRandomPosition(PhysicalEntity entity) {
		float x = NumberFunctions.rndFloat(2, mapSize-3);
		float z = NumberFunctions.rndFloat(2, mapSize-3);
		this.actuallyAddEntity(entity);
		long start = System.currentTimeMillis();
		while (entity.simpleRigidBody.checkForCollisions(false).size() > 0) {
			if (System.currentTimeMillis() - start > 5000) {
				throw new RuntimeException("Taking too long to place an entity");
			}
			x = NumberFunctions.rndFloat(2, mapSize-3);
			z = NumberFunctions.rndFloat(2, mapSize-3);
			entity.setWorldTranslation(x, z);
		}
		// randomly rotate
		JMEAngleFunctions.rotateToWorldDirectionYAxis(entity.getMainNode(), NumberFunctions.rnd(0,  359));
	}


	@Override
	protected AbstractServerAvatar createPlayersAvatarEntity(ClientData client, int entityid) {
		WizardServerAvatar avatar = new WizardServerAvatar(this, client, client.remoteInput, entityid);

		IAbility abilityGun = new MagicWand(this, getNextEntityID(), client.getPlayerID(), avatar, entityid, (byte)0, client);
		this.actuallyAddEntity(abilityGun);

		return avatar;
	}


	@Override
	protected byte getWinningSideAtEnd() {
		int highestScore = -1;
		byte winningSide = -1;
		boolean draw = false;
		for(ClientData c : this.clientList.getClients()) {
			MTDSimplePlayerData spd = (MTDSimplePlayerData)c.playerData;
			if (spd.score > highestScore) {
				winningSide = spd.side;
				highestScore = spd.score;
				draw = false;
			} else if (spd.score == highestScore) {
				draw = true;
			}
		}
		if (draw) {
			return -1;
		}
		return winningSide;
	}


	@Override
	protected Class<? extends Object>[] getListofMessageClasses() {
		return new Class[] {MTDSimplePlayerData.class};
	}


	@Override
	public byte getSideForPlayer(ClientData client) {
		return (byte) client.getPlayerID(); // Everyone is on a different side.  Todo - check  !> 127
	}


	@Override
	public int getMinSidesRequiredForGame() {
		return 1;
	}


	@Override
	protected SimplePlayerData createSimplePlayerData() {
		return new MTDSimplePlayerData();
	}


}

