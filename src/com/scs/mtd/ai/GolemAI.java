package com.scs.mtd.ai;

import com.jme3.math.Vector3f;
import com.scs.mtd.entities.Floor;
import com.scs.mtd.entities.Golem;
import com.scs.mtd.entities.WizardServerAvatar;
import com.scs.stevetech1.components.IEntity;
import com.scs.stevetech1.components.ITargetableByAI;
import com.scs.stevetech1.entities.AbstractAvatar;
import com.scs.stevetech1.entities.PhysicalEntity;
import com.scs.stevetech1.jme.JMEAngleFunctions;
import com.scs.stevetech1.server.AbstractGameServer;
import com.scs.stevetech1.server.Globals;
import com.scs.stevetech1.server.IArtificialIntelligence;

import ssmith.lang.NumberFunctions;
import ssmith.util.RealtimeInterval;

public class GolemAI implements IArtificialIntelligence {

	private static final float VIEW_RANGE = 30f;
	private static final float SPEED = 0.2f;

	private Golem golemEntity;
	private RealtimeInterval checkForEnemyInt;
	private ITargetableByAI currentTarget;
	private int animCode = 0;
	private float maintainDirectionForSecs = 0; // e.g. collided with comrade

	private float speed;
	//private Vector3f tmpDir = new Vector3f();
	private Vector3f tmpMove = new Vector3f();

	public GolemAI(Golem _pe) {
		super();

		golemEntity = _pe;
		checkForEnemyInt = new RealtimeInterval(NumberFunctions.rnd(1200,  2000)); // Avoid AI all doing everthing at the same time
		speed = SPEED;

		//changeDirection(JMEAngleFunctions.getRandomDirection_8()); // Start us pointing in a direction
	}


	@Override
	public void process(AbstractGameServer server, float tpfSecs) {
		if (this.maintainDirectionForSecs > 0) {
			this.maintainDirectionForSecs -= tpfSecs;
		}

		if (currentTarget != null) { // Check we can still see enemy
			if (!this.currentTarget.isAlive()) {
				this.currentTarget = null; // Current target is dead
			} else {
				boolean cansee = golemEntity.canSee((PhysicalEntity)this.currentTarget, VIEW_RANGE, -1);
				if (!cansee) {
					this.currentTarget = null; // Can't see target any more
				}
			}
		}


		if (this.checkForEnemyInt.hitInterval()) {
			currentTarget = server.getTarget(this.golemEntity, this.golemEntity.side, VIEW_RANGE, -1);
		}

		if (currentTarget != null) {
			// Face target
			PhysicalEntity pe = (PhysicalEntity)this.currentTarget;
			JMEAngleFunctions.turnTowards_Const2(this.golemEntity.getMainNode(), pe.getWorldTranslation(), 1 * tpfSecs);

			golemEntity.simpleRigidBody.getAdditionalForce().set(0, 0, 0); // Stop walking
			animCode = AbstractAvatar.ANIM_IDLE;
		}


		if (currentTarget == null) { // No current enemy
			golemEntity.simpleRigidBody.getAdditionalForce().set(0, 0, 0); // Stop walking
			animCode = AbstractAvatar.ANIM_IDLE;

		} else if (maintainDirectionForSecs > 0) {
			golemEntity.simpleRigidBody.setAdditionalForce(this.golemEntity.getRotation().mult(speed, tmpMove)); // Walk forwards
			animCode = AbstractAvatar.ANIM_WALKING;

		} else {
			golemEntity.simpleRigidBody.setAdditionalForce(this.golemEntity.getRotation().mult(speed, tmpMove)); // Walk forwards
			animCode = AbstractAvatar.ANIM_WALKING;
		}

	}


	@Override
	public void collided(PhysicalEntity pe) {
		if (pe instanceof Floor == false) {
			if (pe instanceof WizardServerAvatar) {
				Globals.p(this + " has collided with " + pe);
				this.animCode = AbstractAvatar.ANIM_ATTACK;
				// todo - face them, damage
			}
		}
	}


	@Override
	public int getAnimCode() {
		return animCode;
	}


	@Override
	public ITargetableByAI getCurrentTarget() {
		return this.currentTarget;
	}


	@Override
	public void wounded(IEntity collider) {
		if (this.golemEntity.getHealth() > 0) {
		}
	}


}
