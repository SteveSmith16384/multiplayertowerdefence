package com.scs.mtd.entities;

import com.scs.mtd.MTDClientEntityCreator;
import com.scs.mtd.models.WizardModel;
import com.scs.stevetech1.entities.AbstractOtherPlayersAvatar;
import com.scs.stevetech1.shared.IEntityController;

public class OtherWizardAvatar extends AbstractOtherPlayersAvatar {
	
	public OtherWizardAvatar(IEntityController game, int eid, float x, float y, float z, byte side, String playerName) {
		super(game, MTDClientEntityCreator.AVATAR, eid, x, y, z, new WizardModel(game.getAssetManager()), side, playerName);
	}


	@Override
	public void setAnimCode_ClientSide(int animCode) {
		model.setAnim(animCode);
	}


}
