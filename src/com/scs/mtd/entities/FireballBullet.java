package com.scs.mtd.entities;

import com.jme3.asset.TextureKey;
import com.jme3.bounding.BoundingBox;
import com.jme3.material.Material;
import com.jme3.math.Vector3f;
import com.jme3.renderer.queue.RenderQueue.ShadowMode;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Sphere;
import com.jme3.scene.shape.Sphere.TextureMode;
import com.jme3.texture.Texture;
import com.scs.mtd.MTDClientEntityCreator;
import com.scs.simplephysics.SimpleRigidBody;
import com.scs.stevetech1.components.IEntity;
import com.scs.stevetech1.components.INotifiedOfCollision;
import com.scs.stevetech1.entities.AbstractBullet;
import com.scs.stevetech1.entities.DebuggingSphere;
import com.scs.stevetech1.entities.PhysicalEntity;
import com.scs.stevetech1.server.ClientData;
import com.scs.stevetech1.server.Globals;
import com.scs.stevetech1.shared.IEntityController;

public class FireballBullet extends AbstractBullet implements INotifiedOfCollision {

	public FireballBullet(IEntityController _game, int id, int playerOwnerId, IEntity _shooter, Vector3f startPos, Vector3f _dir, byte _side, ClientData _client) {
		super(_game, id, MTDClientEntityCreator.FIREBALL_BULLET, "FireballBullet", playerOwnerId, _shooter, startPos, _dir, _side, _client, false, 0f, 0f);
	}


	@Override
	protected void createModelAndSimpleRigidBody(Vector3f dir) {
		Sphere sphere = new Sphere(8, 8, 0.1f, true, false);
		Geometry ball_geo = new Geometry("ball_geo", sphere);

		if (!game.isServer()) {
			sphere.setTextureMode(TextureMode.Projected);
			ball_geo.setShadowMode(ShadowMode.Cast);
			TextureKey key = new TextureKey("Textures/yellowsun.jpg");
			Texture tex = game.getAssetManager().loadTexture(key);
			Material mat = new Material(game.getAssetManager(),"Common/MatDefs/Light/Lighting.j3md");
			mat.setTexture("DiffuseMap", tex);
			ball_geo.setMaterial(mat);
		}

		ball_geo.setModelBound(new BoundingBox());
		this.mainNode.attachChild(ball_geo);

		this.simpleRigidBody = new SimpleRigidBody<PhysicalEntity>(this, game.getPhysicsController(), true, this);
		this.simpleRigidBody.setGravity(0f);
		this.simpleRigidBody.setBounciness(0f);
		this.simpleRigidBody.setAerodynamicness(1f); // Stop us slowing down
		this.simpleRigidBody.setLinearVelocity(dir.normalize().mult(20));
	}


	@Override
	public float getDamageCaused() {
		return 1;
	}


	@Override
	public void notifiedOfCollision(PhysicalEntity pe) {
		if (game.isServer()) {
			if (Globals.SHOW_BULLET_COLLISION_POS) {
				// Create debugging sphere
				Vector3f pos = this.getWorldTranslation();
				DebuggingSphere ds = new DebuggingSphere(game, game.getNextEntityID(), pos.x, pos.y, pos.z, true, false);
				game.addEntity(ds);
			}
		}
		game.markForRemoval(this);
	}


}
