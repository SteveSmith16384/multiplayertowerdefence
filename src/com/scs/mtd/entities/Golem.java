package com.scs.mtd.entities;

import com.scs.mtd.MTDClientEntityCreator;
import com.scs.mtd.ai.GolemAI;
import com.scs.mtd.models.GolemModel;
import com.scs.stevetech1.entities.AbstractMob;
import com.scs.stevetech1.shared.IEntityController;

public class Golem extends AbstractMob {

	public static final float START_HEALTH = 5f;

	public Golem(IEntityController _game, int id, float x, float y, float z, int startAnimCode) {
		super(_game, id, MTDClientEntityCreator.GOLEM, x, y, z, (byte)-1, new GolemModel(_game.getAssetManager()), "Golem", START_HEALTH, startAnimCode);

		if (game.isServer()) {
			this.ai = new GolemAI(this);
		}
	}

}
