package com.scs.mtd.entities;

import com.scs.mtd.MTDClientEntityCreator;
import com.scs.mtd.MTDSimplePlayerData;
import com.scs.mtd.MTDStaticData;
import com.scs.mtd.models.WizardModel;
import com.scs.stevetech1.avatartypes.PersonAvatarControl;
import com.scs.stevetech1.components.IDebrisTexture;
import com.scs.stevetech1.components.IEntity;
import com.scs.stevetech1.components.INotifiedOfCollision;
import com.scs.stevetech1.entities.AbstractServerAvatar;
import com.scs.stevetech1.entities.PhysicalEntity;
import com.scs.stevetech1.input.IInputDevice;
import com.scs.stevetech1.server.AbstractGameServer;
import com.scs.stevetech1.server.ClientData;
import com.scs.stevetech1.shared.IEntityController;

public class WizardServerAvatar extends AbstractServerAvatar {

	public WizardServerAvatar(IEntityController _module, ClientData client, IInputDevice _input, int eid) {
		super(_module, MTDClientEntityCreator.AVATAR, client, _input, eid, new WizardModel(_module.getAssetManager()), MTDStaticData.START_HEALTH, 0, new PersonAvatarControl(_module, _input, MTDStaticData.MOVE_SPEED, MTDStaticData.JUMP_FORCE));
	}


	@Override
	public void getReadyForGame() {
		super.getReadyForGame();

		MTDSimplePlayerData data = (MTDSimplePlayerData)this.client.playerData;
		data.score = 0;
	}


	@Override
	public void damaged(float amt, IEntity collider, String reason) {
		super.damaged(amt, collider, reason);

		if (collider instanceof FireballBullet) {
			FireballBullet sb = (FireballBullet)collider;
			AbstractGameServer server = (AbstractGameServer)game;
			server.sendExplosionShards(sb.getWorldTranslation(), 12, .8f, 1.2f, .005f, .02f, "Textures/yellowsun.jpg");
			this.avatarControl.jump(); // Also make them jump
		}
	}


	@Override
	protected void setDied(IEntity killer, String reason) {
		super.setDied(killer, reason);

		if (killer != null && killer instanceof WizardServerAvatar) {
			WizardServerAvatar csp = (WizardServerAvatar)killer;
			csp.incScore(1);
		}
	}


	private void incScore(int i) {
		MTDSimplePlayerData data = (MTDSimplePlayerData)this.client.playerData;
		data.score += i;
		AbstractGameServer server = (AbstractGameServer)game;
		server.sendSimpleGameDataToClients();
	}


}
