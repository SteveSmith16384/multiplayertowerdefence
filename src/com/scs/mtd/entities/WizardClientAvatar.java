package com.scs.mtd.entities;

import com.jme3.renderer.Camera;
import com.scs.mtd.MTDClientEntityCreator;
import com.scs.mtd.MTDStaticData;
import com.scs.mtd.models.WizardModel;
import com.scs.stevetech1.avatartypes.PersonAvatarControl;
import com.scs.stevetech1.client.AbstractGameClient;
import com.scs.stevetech1.client.IClientApp;
import com.scs.stevetech1.entities.AbstractClientAvatar;
import com.scs.stevetech1.input.IInputDevice;

import ssmith.util.RealtimeInterval;

public class WizardClientAvatar extends AbstractClientAvatar {

	public WizardClientAvatar(AbstractGameClient _module, int _playerID, IInputDevice _input, Camera _cam, int eid, float x, float y, float z, byte side) {
		super(_module, MTDClientEntityCreator.AVATAR, _playerID, _input, _cam, eid, x, y, z, side, new WizardModel(_module.getAssetManager()), new PersonAvatarControl(_module, _input, MTDStaticData.MOVE_SPEED, MTDStaticData.JUMP_FORCE));
	}


}
