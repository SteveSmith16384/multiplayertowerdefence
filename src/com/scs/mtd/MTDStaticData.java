package com.scs.mtd;

public class MTDStaticData {

	public static final int DEFAULT_PORT = 6144;
	public static final boolean USE_MODELS_FOR_COLLISION = true;
	public static final float START_HEALTH = 100f;
	public static final float MOVE_SPEED = 3f;
	public static final float JUMP_FORCE = 2.5f;
	
}
