package com.scs.mtd;

import com.jme3.math.Vector3f;
import com.scs.mtd.entities.FireballBullet;
import com.scs.mtd.entities.Floor;
import com.scs.mtd.entities.Golem;
import com.scs.mtd.entities.InvisibleMapBorder;
import com.scs.mtd.entities.OtherWizardAvatar;
import com.scs.mtd.entities.WizardClientAvatar;
import com.scs.mtd.weapons.MagicWand;
import com.scs.stevetech1.client.AbstractGameClient;
import com.scs.stevetech1.components.IEntity;
import com.scs.stevetech1.entities.AbstractClientAvatar;
import com.scs.stevetech1.entities.AbstractOtherPlayersAvatar;
import com.scs.stevetech1.entities.DebuggingSphere;
import com.scs.stevetech1.entities.ExplosionShard;
import com.scs.stevetech1.netmessages.NewEntityData;
import com.scs.stevetech1.server.Globals;

public class MTDClientEntityCreator {

	public static final int AVATAR = 1;
	public static final int FLOOR = 2;
	public static final int MAGIC_WAND = 3;
	public static final int FIREBALL_BULLET = 4;
	public static final int GOLEM = 5;
	public static final int INVISIBLE_MAP_BORDER = 6;
	public static final int STATIC_WALL = 7;

	public MTDClientEntityCreator() {
		super();
	}


	public IEntity createEntity(AbstractGameClient game, NewEntityData msg) {
		int id = msg.entityID;

		Vector3f pos = (Vector3f)msg.data.get("pos");

		switch (msg.type) {
		case AVATAR:
		{
			int playerID = (int)msg.data.get("playerID");
			byte side = (byte)msg.data.get("side");
			String playersName = (String)msg.data.get("playersName");

			if (playerID == game.playerID) {
				AbstractClientAvatar avatar = new WizardClientAvatar(game, id, game.input, game.getCamera(), id, pos.x, pos.y, pos.z, side);
				return avatar;
			} else {
				// Create a simple avatar since we don't control these
				AbstractOtherPlayersAvatar avatar = new OtherWizardAvatar(game, id, pos.x, pos.y, pos.z, side, playersName);
				return avatar;
			}
		}

		case FLOOR:
		{
			Vector3f size = (Vector3f)msg.data.get("size");
			String tex = (String)msg.data.get("tex");
			Floor floor = new Floor(game, id, pos.x, pos.y, pos.z, size.x, size.y, size.z, tex);
			return floor;
		}

		case MAGIC_WAND: 
		{
			int ownerid = (int)msg.data.get("ownerid");
			byte num = (byte)msg.data.get("num");
			int playerID = (int)msg.data.get("playerID");
			MagicWand gl = new MagicWand(game, id, playerID, null, ownerid, num, null);
			return gl;
		}

		case FIREBALL_BULLET:
		{
			int playerID = (int) msg.data.get("playerID");
			if (playerID != game.getPlayerID()) {
				byte side = (byte) msg.data.get("side");
				int shooterId =  (int) msg.data.get("shooterID");
				IEntity shooter = game.entities.get(shooterId);
				Vector3f startPos = (Vector3f) msg.data.get("startPos");
				Vector3f dir = (Vector3f) msg.data.get("dir");
				FireballBullet fireball = new FireballBullet(game, game.getNextEntityID(), playerID, shooter, startPos, dir, side, null); // Notice we generate our own ID
				return fireball;
			} else {
				return null; // it's our bullet, which we've already created locally
			}
		}

		case GOLEM:
		{
			int animCode = (int) msg.data.get("animcode");
			Golem z = new Golem(game, id, pos.x, pos.y, pos.z, animCode);
			return z;
		}

		case INVISIBLE_MAP_BORDER:
		{
			Vector3f dir = (Vector3f)msg.data.get("dir");
			float size = (float)msg.data.get("size");
			InvisibleMapBorder hill = new InvisibleMapBorder(game, id, pos.x, pos.y, pos.z, size, dir);
			return hill;
		}
/*
		case STATIC_WALL:
		{
			Quaternion q = (Quaternion)msg.data.get("quat");
			StaticWall igloo = new StaticWall(game, id, pos.x, pos.y, pos.z, q);
			return igloo;
		}
*/
		case Globals.DEBUGGING_SPHERE:
		{
			DebuggingSphere hill = new DebuggingSphere(game, id, pos.x, pos.y, pos.z, true, false);
			return hill;
		}

		case Globals.EXPLOSION_SHARD:
		{
			Vector3f forceDirection = (Vector3f) msg.data.get("forceDirection");
			float size = (float) msg.data.get("size");
			String tex = (String) msg.data.get("tex");
			ExplosionShard expl = new ExplosionShard(game, pos.x, pos.y, pos.z, size, forceDirection, tex);
			return expl;
		}

		default:
			throw new RuntimeException("Unknown entity type: " + msg.type);
		}
	}

}

