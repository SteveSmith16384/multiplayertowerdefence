# Multi-Player Tower Defence

A 3D FPS with Tower-Defence game play.  Read the tutorial at http://multiplayertowerdefence.blogspot.com/

## Compiling Requirements
* Java
* SteveTech1 (https://bitbucket.org/SteveSmith16384/stetech1)

If you are trying to compile this, you'll probably want to use the development branch of SteveTech1, since that's what I usually develop against.


## Credits
Written by Steve Smith ( http://twitter.com/stephencsmith/ )


### Model Credits
Mage by Clement Wu, Nikolaus & Botanic (taken from https://opengameart.org/content/animated-mage)
Lava Golem by gavlig (taken from https://opengameart.org/content/lava-golem)
Slime Mold by Grefuntor (taken from https://opengameart.org/content/slime-mold)
Wyvern by Skorpio (taken from https://opengameart.org/content/wyvern-0)

